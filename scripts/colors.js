import fs from 'node:fs/promises';

import c from '@evilmartians/harmony/base';

/**
 *
 * @param {string} name
 * @param {Record<number, string>} scale
 */
function scale(name, scale) {
	return Object.entries(scale).map(
		([k, v]) => `--color-${name}-${k}:${v};`,
	);
}

const css = `:root {
	${scale('neutral', c.neutral).join('\n\t')}
	${scale('accent', c.indigo).join('\n\t')}
	[data-status=info] {
		${scale('status', c.indigo).join('\n\t')}
	}
	[data-status=warning] {
		${scale('status', c.amber).join('\n\t')}
	}
	[data-status=danger] {
		${scale('status', c.red).join('\n\t')}
	}
	[data-status=success] {
		${scale('status', c.emerald).join('\n\t')}
	}
}`

await fs.mkdir('dist/css', { recursive: true });
await fs.writeFile('dist/css/colors.css', css, 'utf-8');
