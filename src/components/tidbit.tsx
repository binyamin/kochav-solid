import clsx from 'clsx';
import { type ComponentProps, createMemo, mergeProps, splitProps } from 'solid-js';
import { Icon, type IconProps } from '@kochav/icons';

import './tidbit.css';

// TODO should we expose more props from "aside"?
interface Props extends Pick<
	ComponentProps<'aside'>,
	| '$ServerOnly'
	| 'ref'
	| 'class'
	| 'classList'
	| 'children'
> {
	/**
	 * @default 'info'
	 */
	status?: 'success' | 'info' | 'warning' | 'danger';
	/**
	 * @default // Generated from `status`
	 */
	label?: string;
}

const iconName = {
	info: 'info-circle-outline',
	warning: 'alert-triangle-outline',
	danger: 'alert-circle-outline',
	success: 'check-circle-outline',
} satisfies Record<NonNullable<Props['status']>, IconProps['name']>;

export function Tidbit(props: Props) {
	const [local, domProps] = splitProps(
		mergeProps({
			status: 'info' as const,
		}, props),
		['status', 'class', 'label', 'children'],
	);

	const label = createMemo(() => {
		if (local.status === 'danger') {
			return 'Error';
		}

		return local.status[0].toUpperCase() + local.status.slice(1);
	});

	return <aside
		{...domProps}
		class={clsx("tidbit", 'l-sidebar', local.class)}
		data-status={local.status}
	>
		<Icon name={iconName[local.status]} label={label()} />

		<div>
			<p class='_title'>{local.label ?? label()}</p>
			<div>{local.children}</div>
		</div>
	</aside>
}
