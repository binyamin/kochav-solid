import clsx from 'clsx';
import { type ComponentProps, mergeProps, splitProps } from 'solid-js';
import './separator.css';

interface Props extends ComponentProps<'div'> {
	/**
	 * Either `vertical` or `horizontal`.
	 * @default 'horizontal'
	 */
	orientation?: 'horizontal' | 'vertical';
	/**
	 * Whether or not the component is purely decorative. When true, accessibility-related attributes
	 * are updated so that that the rendered element is removed from the accessibility tree.
	 * @default false
	 */
	decorative?: boolean;
}

/**
 * Visually or semantically separates content.
 * @see {@link https://www.radix-ui.com/primitives/docs/components/separator}
 */
export function Separator(props: Props) {
	const [local, dom] = splitProps(mergeProps({
		orientation: 'horizontal' as const,
		decorative: false,
	}, props), ['orientation', 'decorative', 'class']);

	const semanticProps = local.decorative
	? { role: 'none' as const }
	: { 'aria-orientation': local.orientation, role: 'separator' as const };

	return <div
		data-orientation={local.orientation}
		{...mergeProps(semanticProps, dom)}
		class={clsx('separator', local.class)}
	/>
}
