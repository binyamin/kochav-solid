import clsx from 'clsx';
import { type ComponentProps, createMemo, mergeProps, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import './button.css';

type Props<T extends 'button' | 'a'> = ComponentProps<T> & {
	/**
	 * Visual style
	 * @default 'solid'
	 */
	variant?: 'solid' | 'soft' | 'outline' | 'text';

	/**
	 * Only applies when {@linkcode variant} is either `solid`, `soft`, or `text`.
	 *
	 * @default 'neutral'
	 */
	status?: 'info' | 'danger' | 'neutral';
}

function createButton<T extends 'button' | 'a'>(as: T) {
	return function (props: Props<T>) {
		const [local_, domProps] = splitProps(
			mergeProps({ variant: 'solid' }, props),
			['status', 'variant', 'class'],
		);

		const local = local_ as Pick<Props<T>, 'status' | 'variant'> & { class?: string };

		const status = createMemo(() => {
			if (local.status === 'neutral') {
				return undefined;
			}

			return local.status;
		});

		return <Dynamic
			component={as}
			{...domProps as ComponentProps<T>}
			class={clsx('button', local.class)}
			data-status={status()}
			data-variant={local.variant}
		/>
	}
}

export const Button = createButton('button');
export const LinkButton = createButton('a');
